class Appmtn {
  String? name;
  String? sector;
  String? developer;
  int? year;

  void printAppInfo() {
    name = name?.toUpperCase();
    print("Name of the app is $name");
    print("The sector/category of the app is $sector");
    print("The developer of the app is $developer");
    print('The app was develop in $year');
  }
}

void main(List<String> args) {
  var Winner = Appmtn();
  Winner.name = 'EasyEquity';
  Winner.sector = 'Investment Sector';
  Winner.developer = 'Charles Savage';
  Winner.year = 2020;

  Winner.printAppInfo();
}
